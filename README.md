# PDT-Servico de autenticação e autorização utilizando o software FusionAuth

FusionAuth providencia autenticações e autorizações e gerenciamento de usuários para qualquer aplicação

[Para saber mais sobre o sistema FusionAuth acesse o site oficial clicando aqui](https://fusionauth.io/)

# Configuração o FusionAuth utilizando o docker-compose
## Descrição das variáveis de ambiente 
### Variáveis do service de banco de dados  

| Variável | Valor |
|----|----|
| POSTGRES_USER | usuário root do banco postgres
| POSTGRES_PASSWORD | senha do usuario root do banco
| PGDATA | diretorio onde ficaram armazenados os arquivos que compoem o banco

### Serviço FusionAuth

| Variável | Valor |
|----|----|
|DATABASE_URL |jdbc string url
|DATABASE_ROOT_USER | nome do usuário root do banco
|DATABASE_ROOT_PASSWORD |senha do usuário root do banco
|DATABASE_USER |nome do usuário que será criado para o banco
|DATABASE_PASSWORD | senha de acesso para o usuário criado 
|FUSIONAUTH_MEMORY | quantidade de memória que será alocado para o JAVA VM onde este serviço irá rodar usar **M** - para megabytes e **G**- para Gigabytes
|FUSIONAUTH_SEARCH_ENGINE_TYPE | engine de pesquisa possíveis válores **database** ou **elasticserach**
|FUSIONAUTH_RUNTIME_MODE | **development** e **production** usar de acordo com o ambiente   